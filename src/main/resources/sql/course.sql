create table if not exists instructor_detail
(
    uuid            uuid primary key unique not null,
    youtube_channel varchar(128),
    hobby           varchar(45)
);

create table if not exists instructor
(
    uuid                   uuid primary key unique not null,
    first_name             varchar(45),
    last_name              varchar(45),
    email                  varchar(45),
    instructor_detail_uuid uuid,
    FOREIGN KEY (instructor_detail_uuid) REFERENCES instructor_detail (uuid) on delete cascade
);

create table if not exists course
(
    uuid            uuid primary key unique not null,
    title           varchar(128),
    instructor_uuid uuid,
    FOREIGN KEY (instructor_uuid) REFERENCES instructor (uuid)
);

create table if not exists review
(
    uuid        uuid primary key unique not null,
    comment     varchar(255),
    course_uuid uuid,
    FOREIGN KEY (course_uuid) REFERENCES course (uuid)
);

create table if not exists student
(
    uuid       uuid primary key unique not null,
    first_name varchar(45),
    last_name  varchar(45),
    email      varchar(45)
);

create table if not exists course_student
(
    course_uuid  uuid primary key not null,
    student_uuid uuid primary key not null
);
