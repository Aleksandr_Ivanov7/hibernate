package dao;

import entity.Course;
import org.hibernate.Session;

import java.util.List;
import java.util.UUID;

public class CourseDao extends Dao<Course> {

    public void create(Course course) {
        super.create(course);
    }

    public Course get(UUID uuid) {
        return super.get(Course.class, uuid);
    }

    public List<Course> getAll() {
        return super.getAll(Course.class);
    }

    public void update(UUID uuid, Course course) {
        super.update(Course.class, uuid, course);
    }

    public void delete(UUID uuid) {
        super.delete(Course.class, uuid);
    }
}
