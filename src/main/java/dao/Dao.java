package dao;

import entity.Course;
import entity.InfoEntity;
import entity.Instructor;
import entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.UUID;

public abstract class Dao<E extends InfoEntity> {
    private SessionFactory factory;
    private Session session;

    public Dao() {
        factory = new Configuration()
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();
        session = factory.getCurrentSession();
    }

    public void create(E e) {
        session.save(e);
    }

    public E get(Class<E> clazz, UUID uuid) {
        return session.get(clazz, uuid);
    }

    public List<E> getAll(Class<E> clazz) {
        List<E> list = session.createQuery("from " + clazz.getName()).list();
        return list;
    }

    public void update(Class<E> clazz, UUID uuid, E e) {
        e.setUuid(uuid);
        session.update(e);
    }

    public void delete(Class<E> clazz, UUID uuid) {
        session.createQuery(String.format("delete %s where uuid='%s'",clazz.getName(),uuid.toString())).executeUpdate();
    }

    public Session getSession() {
        return session;
    }
}
