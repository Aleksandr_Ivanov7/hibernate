package dao;

import entity.InstructorDetail;
import org.hibernate.Session;

import java.util.List;
import java.util.UUID;

public class InstructorDetailDao extends Dao<InstructorDetail> {

    public void create(InstructorDetail instructorDetail) {
        super.create(instructorDetail);
    }

    public InstructorDetail get(UUID uuid) {
        return super.get(InstructorDetail.class, uuid);
    }

    public List<InstructorDetail> getAll() {
        return super.getAll(InstructorDetail.class);
    }

    public void update(UUID uuid, InstructorDetail instructorDetail) {
        super.update(InstructorDetail.class, uuid, instructorDetail);
    }

    public void delete(UUID uuid) {
        super.delete(InstructorDetail.class, uuid);
    }
}
