package dao;

import entity.Instructor;
import org.hibernate.Session;

import java.util.List;
import java.util.UUID;

public class InstructorDao extends Dao<Instructor> {

    public void create(Instructor instructor) {
        super.create(instructor);
    }

    public Instructor get(UUID uuid) {
        return super.get(Instructor.class, uuid);
    }

    public List<Instructor> getAll() {
        return super.getAll(Instructor.class);
    }

    public void update(UUID uuid, Instructor instructor) {
        super.update(Instructor.class, uuid, instructor);
    }

    public void delete(UUID uuid) {
        super.delete(Instructor.class, uuid);
    }
}
