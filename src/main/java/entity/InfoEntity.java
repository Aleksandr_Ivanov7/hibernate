package entity;

import java.util.UUID;

public interface InfoEntity {
    UUID getUuid();
    void setUuid(UUID uuid);
}
